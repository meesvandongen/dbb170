import processing.net.*; 
import java.util.Date;

final int PORT = 8910;
int partsAmount = 100;
int visualizationSize = 500;
int startTime = 9;

color lowActivity = color(230, 230, 230);
color highActivity = color(145, 173, 112);
color stressLevelColor = color(0, 0, 255);
color backgroundColor = color(255, 255, 255);

Client myClient; 
String inString;
//initial A value: keypoint xys
String[] keypoint;
String XYnumbers;
String[] XYs;
int data_length = 2000;
int[] X = new int[data_length];//all X
Float[] Y = new Float[data_length];//all Y
int dataNum = 0;
long baseTimestamp = 0;
float lastX = 0, lastY = 0;
byte[] byteBuffer = new byte[1000];

float[] averages = new float[partsAmount];

void calculateAverage(int timeIndex) {
  float tempAverage = 0;
  for (int deviceId = 0; deviceId < heartBeatData.length; ++deviceId) {
    tempAverage += heartBeatData[deviceId][timeIndex];
  }
  averages[timeIndex] = tempAverage / heartBeatData.length;
}

void setGradient(int x, int y, float w, float h, color c1, color c2 ) {

  noFill();
  for (int i = x; i <= x+w; i++) {
    float inter = map(i, x, x+w, 0, 1);
    color c = lerpColor(c1, c2, inter);
    stroke(c);
    line(i, y, i, y+h);
  }
}

void Legend(int size, int padding) {
  translate(0, padding);
  translate(size * 2, 20);
  fill(0);
  text("Legend", 0, 0);

  translate(0, 30);
  text("Stress Amount", 0, 0);
  translate(0, 20);
  textSize(15);
  text("Bar Length (Y-axis)", 0, 0);
  textSize(20);

  translate(0, 30);
  text("Time", 0, 0);
  translate(0, 20);
  textSize(15);
  text("X-axis", 0, 0);
  textSize(20);

  translate(0, 30);
  text("Average Stress", 0, 0);
  translate(0, 20);
  noStroke();
  fill(stressLevelColor);
  rect(50, -1, 120, 0);
  fill(255, 255, 255);

  translate(0, 30);
  fill(0);

  text("Activity", 0, 0);
  translate(0, 20);
  text("Low", 0, 0);
  setGradient(50, -10, 70, 10, lowActivity, highActivity);
  text("High", 130, 0);


  translate(0, -20);
  translate(0, -30);

  translate(0, -20);
  translate(0, -30);

  translate(0, -20);
  translate(0, -30);

  translate(0, -20);
  translate(0, -30);
  translate(-(size * 2), -20);
  translate(0, -padding);
}

class HorizontalAverage {
  float startAmount;
  float endAmount;

  float offSet;

  int size;
  public float partSize;

  float endPositionX;
  float endPositionY;

  int timeIndex;

  public HorizontalAverage (int _partSize, int _size, int _timeIndex) {
    size = _size;
    timeIndex = _timeIndex;
    partSize = _partSize;
  }

  private float transformAmount(float amount, float multiplier) {
    return amount/multiplier + (1-1/multiplier);
  }

  public void render() {
    float startAmount = timeIndex == 0 ? averages[partsAmount-1] : averages[timeIndex - 1];
    float endAmount = averages[timeIndex];
    stroke(stressLevelColor);
    strokeWeight(2);
    line(0, -startAmount * size, partSize, -startAmount * size);
    strokeWeight(1);
  }
}

class HorizontalPart {
  int size;
  public float innerWidth;
  int participantIndex;
  int timeIndex;


  public HorizontalPart (float _innerWidth, int _size, int _participantIndex, int _timeIndex) {
    innerWidth = _innerWidth;
    size = _size;
    participantIndex = _participantIndex;
    timeIndex = _timeIndex;
  }

  public void render() {
    float stressIndex = heartBeatData[participantIndex][timeIndex];
    fill(lerpColor(lowActivity, highActivity, activityData[participantIndex][timeIndex]));
    noStroke();
    rect(0, 0, innerWidth, -stressIndex * size);
  }
}

class HorizontalGraph {
  int size;
  int padding;
  int nTimePieces;
  float partWidth;
  HorizontalPart[] parts;

  int participantIndex;

  HorizontalGraph(int _size, int _padding, int _nTimePieces, int _nInnerCircles, int _participantIndex)
  {
    size = _size;
    padding = _padding;
    nTimePieces = _nTimePieces;
    participantIndex = _participantIndex;


    parts = new HorizontalPart[heartBeatData[participantIndex].length];
    partWidth = (size-2* padding)/(float)parts.length;

    for (int i = 0; i < heartBeatData[participantIndex].length; i++) {
      parts[i] = new HorizontalPart(partWidth, size - padding * 2, participantIndex, i);
    }
  }

  int getMaxbackgroundRadius()
  {
    return size / 2 - padding / 2;
  }

  public void render()
  {
    translate(padding, size - padding);
    renderParts();
    renderHours();
    renderAverage();
    renderStressLines();
    renderNumber();

    translate(-padding, -size + padding);
  }

  int getBackgroundCenter() {
    return padding + getMaxbackgroundRadius();
  }

  int getBackgroundSizeWithPadding() {
    return size - (padding * 2);
  }

  void renderNumber() {
    translate( getBackgroundSizeWithPadding() / 2, -getBackgroundSizeWithPadding() - 5);
    text(participantIndex, 0, 0);
    translate( -getBackgroundSizeWithPadding() / 2, getBackgroundSizeWithPadding() + 5);
  }     


  void renderStressLines() {
    stroke(color(0, 0, 0));

    for (int i = 0; i < 5; i++) {
      translate(0, -i * getBackgroundSizeWithPadding() / 4);
      fill(0);

      text(i, -15, 5);
      line(0, 0, getBackgroundSizeWithPadding() - 5, 0); 
      translate(0, i * getBackgroundSizeWithPadding() / 4);
    }
  }

  public void renderHours() {
    textSize(20);
    float timePieceWidth = (size - 2 * padding) / nTimePieces;
    for (int i = 0; i < nTimePieces + 1; i++) {
      translate(i * timePieceWidth, 0);
      noFill();
      stroke(color(0, 0, 0));
      line(0, 0, 0, -(size - 2 * padding));
      fill(color(255, 255, 255));
      fill(0);

      text(i + startTime, -8, 20);
      translate(-i * timePieceWidth, - 0);
    }
  }

  void renderParts() {
    for (int i = 0; i < parts.length; i++) {

      HorizontalPart part = parts[i];
      translate(partWidth * i, 0);
      part.render();
      translate(-partWidth * i, 0);
    }
  }

  void renderAverage() {
    for (int i = 0; i < averageObjectList.length; ++i) {
      translate((i -1) * partWidth, 0);
      averageObjectList[i].render();
      translate(-(i-1) * partWidth, 0);
    }
  }
}

class Average {
  float startAmount = 0;
  float endAmount = 0;

  float offSet = 100;

  int size;
  public float angle;

  float endPositionX;
  float endPositionY;

  int timeIndex;

  public Average (float _angle, int _size, int _timeIndex) {
    angle = _angle;
    size = _size;
    timeIndex = _timeIndex;

    generateEndPosition(endAmount, _angle);
  }

  private float transformAmount(float amount, float multiplier) {
    return amount/multiplier + (1-1/multiplier);
  }

  public void generateEndPosition(float endAmount, float angle) {
    endPositionX = sin(angle) * endAmount;
    endPositionY = cos(angle) * endAmount;
  }

  public void findStartAndEndAmount() {
    startAmount = timeIndex == 0 ? averages[partsAmount-1] : averages[timeIndex - 1];
    endAmount = averages[timeIndex];
    generateEndPosition(endAmount, angle);
  }

  public void render() {
    stroke(stressLevelColor);
    line(0, -startAmount * (size / 2), endPositionX * (size / 2), -endPositionY * (size / 2 ));
  }
}

HorizontalAverage[] averageObjectList = new HorizontalAverage[partsAmount];


class Part {
  int size;
  public float angle;
  int participantIndex;
  int timeIndex;


  public Part (float _angle, int _size, int _participantIndex, int _timeIndex) {
    angle = _angle;
    size = _size;
    participantIndex = _participantIndex;
    timeIndex = _timeIndex;
  }

  public void render() {
    float stressIndex = heartBeatData[participantIndex][timeIndex];
    fill(lerpColor(lowActivity, highActivity, activityData[participantIndex][timeIndex]));
    rotate(radians(-90));
    noStroke();
    arc(0, 0, stressIndex * size, stressIndex * size, 0, angle);
    rotate(radians(90));
  }
}


class Visualization
{

  int size;
  int padding;
  int nInnerCircles;
  int nTimePies;
  float partAngle;
  Part[] parts;

  Visualization(int _size, int _padding, int _nTimePies, int _nInnerCircles, int participantIndex)
  {
    size = _size;
    padding = _padding;
    nInnerCircles = _nInnerCircles;
    nTimePies = _nTimePies;

    parts = new Part[heartBeatData[participantIndex].length];
    partAngle = radians((1/(float)(parts.length)) * 360);
    for (int i = 0; i < heartBeatData[participantIndex].length; i++) {
      parts[i] = new Part(partAngle, size - padding * 2, participantIndex, i);
    }
  }

  int getMaxbackgroundRadius()
  {
    return size / 2 - padding / 2;
  }

  public void render()
  {
    translate(getBackgroundCenter(), getBackgroundCenter());
    renderParts();
    renderBackground();
    renderHours();
    renderAverage();
    translate(-getBackgroundCenter(), -getBackgroundCenter());
  }

  int getBackgroundCenter() {
    return padding + getMaxbackgroundRadius();
  }

  void renderBackground()
  {
    noFill();
    stroke(#000000);
    for (int i = 1; i < nInnerCircles; ++i) {
      circle(
        0, 
        0, 
        getMaxbackgroundRadius() * 2 * ((float)i/nInnerCircles)
        );
    }
  }

  public void renderHours() {
    float  angle = radians((1/(float)(nTimePies)) * 360);
    for (int i = 0; i < nTimePies; i++) {
      rotate(radians(-90));
      rotate(i * angle);
      noFill();
      arc(0, 0, size - padding * 2, size - padding * 2, 0, angle, PIE);
      rotate(-i * angle);
      rotate(radians(90));
    }
  }

  void renderParts() {
    for (int i = 0; i < parts.length; i++) {

      Part part = parts[i];
      rotate(partAngle * i);
      part.render();
      rotate(-partAngle * i);
    }
  }

  void renderAverage() {
    for (int i = 0; i < averageObjectList.length; ++i) {
      rotate(partAngle * i);
      averageObjectList[i].render();
      rotate(-partAngle * i);
    }
  }
};


HorizontalGraph[] visualizations = new HorizontalGraph[4];
float[][] heartBeatData = new float[4][];
float[][] activityData = new float[4][];

int pointer = 0;

FloatList[] Short_Activity = new FloatList[4];
FloatList[] Short_SDNN = new FloatList[4];



void setup() {
  size(1200, 1000);
  int size = 500;
  int padding = 40;

  rectMode(CORNERS);


  for (int i = 0; i < 4; i ++) {
    heartBeatData[i] = new float[partsAmount];
    activityData[i] = new float[partsAmount];

    Short_Activity[i] = new FloatList();
    Short_SDNN[i] = new FloatList();
  }


  myClient = new Client(this, "192.168.43.32", PORT);
  for (int i = 0; i < visualizations.length; i++) {
    visualizations[i] = new HorizontalGraph(size, padding, 8, 4, i);
  }
  for (int i = 0; i < averageObjectList.length; ++i) {
    //averageObjectList[i] = new Average(radians((1/(float)(partsAmount)) * 360), size - 2* padding, i);
    averageObjectList[i] = new HorizontalAverage((size - 2* padding)/partsAmount, size - 2* padding, i);
  }
}

long timerTime = 0;
int timerPointer = 0;

void processActivityAndSdnn() {
  for (int i = 0; i < Short_SDNN.length; ++i) {
    float tempSdnn = 0;

    FloatList shortDeviceActivity = Short_SDNN[i];
    println("processing", shortDeviceActivity.size(), " SDNN numbers for device, ", i);

    for (int j = 0; j < shortDeviceActivity.size(); ++j) {
      tempSdnn += shortDeviceActivity.get(j);
    }
    tempSdnn = tempSdnn / shortDeviceActivity.size();
    shortDeviceActivity.clear();
    // Divide by 100 to make in range 0-1. more or less.
    // TODO: Change it so that more stress means longer bar. Change to categorical (stress/no stress). If value is undefined, take value of last timerPointer.
    if (Float.isNaN(tempSdnn)) {
      if (timerPointer > 0) {
        heartBeatData[i][timerPointer] = heartBeatData[i][timerPointer-1];
      } else {
        heartBeatData[i][timerPointer] = 0;
      }
    } else {

      heartBeatData[i][timerPointer] =  tempSdnn > 100 ? 0 :  tempSdnn > 75 ? 0.25 : tempSdnn > 50 ? 0.5 : tempSdnn > 25 ? 0.75 : 1;
    }
  }

  for (int i = 0; i < Short_Activity.length; ++i) {
    float tempActivity = 0;
    FloatList shortDeviceActivity = Short_Activity[i];
    println("processing", shortDeviceActivity.size(), " Activity numbers for device, ", i);
    for (int j = 0; j < shortDeviceActivity.size(); ++j) {
      tempActivity += shortDeviceActivity.get(j);
    }
    tempActivity = tempActivity / shortDeviceActivity.size();
    shortDeviceActivity.clear();
    activityData[i][timerPointer] = Float.isNaN(tempActivity) ? 0 : tempActivity;
  }
  calculateAverage(timerPointer);

  timerPointer = timerPointer == partsAmount -1 ? 0 : timerPointer +1;
}


void readBands() {
  if (myClient.available() > 0) { 

    byteBuffer = new byte[1000];

    int byteCount = myClient.readBytesUntil(10, byteBuffer); 
    if (byteCount > 0) {
      String s = new String(byteBuffer, 0, byteCount - 1);
      long timestamp = Long.parseLong(s.split(":")[0]);

      if (timerTime == 0) {
        timerTime = timestamp;
      }


      if (timerTime < timestamp) {
        processActivityAndSdnn();
        timerTime += 20; // every 20 seconds
      }

      String header = s.split(":")[1]; //A/B
      int deviceId = Integer.parseInt(s.split(":")[2]);
      String value = s.split(":")[3];

      if (header.equals("A")) {

        //println("[Receive A]", timestamp, header, deviceId, value);

        if (value.trim().equals("[]")) {
          //println("RECEIVED AN EMPTY MOTION VALUE?");
          return;
        }

        value = value.replace('[', ' ');
        value = value.replace(']', ' ');
        //String XYnumbers = join(value, " ");//mix all XY numbers together
        String[] XYs = splitTokens(value, (","));//split each lines by all kinds of punctuations

        dataNum = XYs.length;
        //println("datanumber length is:", dataNum);
        float totalActivity = 0;
        for (int i = 0; i<XYs.length; i++) {
          int n = i/2;
          if (i%2==0) {
            XYs[i].trim();
          } else {
            Y[n] = float(XYs[i]);
            totalActivity += abs(float(XYs[i]));
          }
        }
        Short_Activity[deviceId].append(totalActivity);
      } else if (header.equals("B")) {
        int sdnn = Integer.parseInt(value);

        Short_SDNN[deviceId].append(sdnn);
      }
    }
  }
}

void readFakeBands() {
  if (frameCount % 2 == 0) {
    for (int i = 0; i < 4; i++) {
      Short_SDNN[i].append(random(0, 100));
      Short_Activity[i].append(random(0, 1));
    }
  }
  if (frameCount % 10 == 0) {
    processActivityAndSdnn();
  }
}

void renderVisualizations() {
  int size = 500;

  for (int x = 0, i = 0; x < 2; x++) {
    for (int y = 0; y < 2; y++) {
      translate(x * size, y * size);
      visualizations[i].render();
      translate(-x * size, -y * size);
      i++;
    }
  }
}

void findAverageLines() {
  for (int i = 0; i < averageObjectList.length; ++i) {
    //averageObjectList[i].findStartAndEndAmount();
  }
}

void draw() {

  background(backgroundColor);


  readBands();
  //readFakeBands();
  //findAverageLines();
  renderVisualizations();
  Legend(500, 40);
}
